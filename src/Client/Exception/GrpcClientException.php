<?php

declare(strict_types=1);

namespace Newcreate\Grpc\Client\Exception;

class GrpcClientException extends \RuntimeException
{
}
