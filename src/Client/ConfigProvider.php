<?php

declare(strict_types=1);

namespace Newcreate\Grpc\Client;

class ConfigProvider
{
    public function __invoke(): array
    {
        return [];
    }
}
